#include<stdio.h>
    struct student 
    {
        int roll_no;
        char name[100];
        char sec[10];
        char dept[40];
        int fees;
        int tot_marks;
    }stud1,stud2;
    
    struct student input(void)
   
    {
        struct student stud;
        printf("\n enter the roll number:");
        scanf("%d",&stud.roll_no);
        printf("\n enter  the name:");
        gets(stud.name);
        printf("\n enter section:");
        gets(stud.sec);
        printf("\n enter department:");
        gets(stud.dept);
        printf("\n enter the fees:");
        scanf("%d",&stud.fees);
        printf("\n enter the total marks obtained:");
        scanf("%d",&stud.tot_marks);
        return stud;
    }
    void output(struct student stu)
    {
        printf("\n ROLL NO.    = %d",stu.roll_no);
        printf("\n NAME        = %s",stu.name);
        printf("\n SECTION     = %s",stu.sec);
        printf("\n DEPARTMENT  = %s",stu.dept);
        printf("\n FEES        = %d",stu.fees);
        printf("\n TOTAL MARKS = %d",stu.tot_marks);
    }
   int main()
   {
   printf("enter first student details:\n");
   stud1 = input();
   printf("enter second student details:\n");
   stud2 = input();
   if(stud1.tot_marks > stud2.tot_marks)
   {
     output(stud1);
   }
   else
   {
     output(stud2);
   }
   return 0;
}