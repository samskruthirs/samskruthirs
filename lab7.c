#include<stdio.h>
int main()
{
  int i, j, mat[3][3], transposed_mat[3][3];
  printf("enter the elements of the matrix\n");
  for(i=0; i<3; i++)
  {
    for(j=0; j<3; j++)
    {
      printf("mat[%d][%d] = ", i, j);
      scanf("%d", &mat[i][j]);
    }
  }
  printf("the elements of the matrix are");
  for(i=0; i<3; i++)
  {
    printf("\n");
    for(j=0; j<3; j++)
    {
      printf("\t mat[%d][%d] = %d", i, j, mat[i][j]);
    }
  }
  for(i=0; i<3; i++)
  {
    for(j=0; j<3; j++)
    {
      transposed_mat[i][j] = mat[j][i];
    }
  }
  printf("\n the elements of the transposed matrix are");
  for(i=0; i<3; i++)
  {
    printf("\n");
    for(j=0; j<3; j++)
    {
      printf("\t transposed_mat[%d][%d] = %d", i, j, transposed_mat[i][j]);
    }
  }
  return 0;
}