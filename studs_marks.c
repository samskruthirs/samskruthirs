#include<stdio.h>
#include<conio.h>
#include<string.h>

    struct marks
    {
        int sub1;
        int sub2;
        int sub3;
    };
    struct student
    {
        int USN;
        char name[100];
        struct marks m;
    };
    void main()
    {
      int n,i,j,c = 0;
      printf("enter number of students:\n");
      scanf("%d",&n);
      printf("\n enter details of %d students:",n);
      struct student stud[n];
      for(i=0;i<n;i++)
      {
         printf("\n STUDENT %d",i+1);
         printf("\n enter USN :");
         scanf("%d",&stud[i].USN);
         printf("\n enter name :");
         scanf("%s",&stud[i].name);
         printf("enter the marks of 3 subjects :");
         scanf("%d %d %d",&stud[i].m.sub1,&stud[i].m.sub2,&stud[i].m.sub3);
      }
      printf("student details\n");
      for(i=0;i<n;i++)
      {
        printf("STUDENT %d\n",i+1);
        printf("USN                 : %d\n",stud[i].USN);
        printf("name                : %s\n",stud[i].name);
        printf("marks of 3 subjects : %d %d %d\n",stud[i].m.sub1,stud[i].m.sub2,stud[i].m.sub3);
      }
      for(i=0;i<n;i++)
      {
        if(stud[i].m.sub1<10 || stud[i].m.sub2<10 || stud[i].m.sub3<10)
        c++;
      }
      if(c==0)
      printf("\n none of the students have scored less than 10 in any subject");
      else if(c>0)
      {
        printf("\n details of STUDENTS with LOWEST MARKS");
        for(i=0;i<n;i++)
        {
          if(stud[i].m.sub1<10 || stud[i].m.sub2<10 || stud[i].m.sub3<10)
          {
            printf("STUDENT %d\n",i+1);
            printf("name   :%s\n",stud[i].name);
          }
        }
     }
 }