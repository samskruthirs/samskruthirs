#include<stdio.h>

struct employee{
    char empname[30];
    int empId;
    float salary;
};

int main()
{

    struct employee emp;
    
    printf("\n enter details:");
    printf("\n name:");
    gets(emp.empname);
    printf("\n Id:");
    scanf("%d", &emp.empId);
    printf("\n salary:");
    scanf("%f",&emp.salary);
    
    printf("\n entered detail is:");
    printf("\n name:%s"    , emp.empname);
    printf("\n empId:%d"   , emp.empId);
    printf("\n salary:%f\n", emp.salary);
    return 0;
}