#include<stdio.h>
void swap(int *x,int *y)
{
    int t;
    
    t = *x;
    *x = *y;
    *y = t;
}
int main()
{
    int n1, n2;
    
    printf("enter the value of n1:\n");
    scanf("%d",&n1);
    printf("enter the value of n2:\n");
    scanf("%d",&n2);
    
    printf("before swapping: n1 is: %d, n2 is: %d\n",n1,n2);
    swap(&n1,&n2);
    printf("after swapping: n1 is: %d, n2 is: %d\n",n1,n2);
    return 0;
}